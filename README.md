# irobot-roulette

## Getting started

Build it!

```bash
$ meson build/
$ ninja -C build/
```

Run it!

```bash
$ ./build/src/app
```

## Output

For debugging purposes, each iteration will print to standard output the
following:

```bash
Iteration 58
Roulette num: 17
        Red:    lost   12      -117     [ 1 2 3 4 ]
        Black:  won    38        63     [ 9 11 20 29 38 ]
        High:   lost   15       -62     [ 5 7 ]
        Low:    won     6       -79     [ 2 3 4 6 ]
        Even:   lost  261       512     [ 63 72 81 90 99 153 ]
        Odd:    won     9      -105     [ 3 4 5 6 9 ]
Total balance: 212
```

Above you can find:
* Iteration number.
* The roulette (pseudo-random) number for this iteration.
* Each player will be listed with its nick name, followed by the bet result.
Also, player's balance and internal bet list state are printed, after applying
the result of the current bet.
* Total balance among all players.
