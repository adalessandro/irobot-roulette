#include <iostream>

#include "player.h"
#include "roulette.h"

#define NUMBER_OF_ITERATIONS		10000

int main(int argc, char **argv)
{
	Roulette r;

	r.addPlayer(std::make_unique<PlayerRed>("Red"));
	r.addPlayer(std::make_unique<PlayerBlack>("Black"));
	r.addPlayer(std::make_unique<PlayerHigh>("High"));
	r.addPlayer(std::make_unique<PlayerLow>("Low"));
	r.addPlayer(std::make_unique<PlayerEven>("Even"));
	r.addPlayer(std::make_unique<PlayerOdd>("Odd"));

	for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
		std::cout << "Iteration " << i << std::endl;
		r.run();
		r.printTotals();
		std::cout << std::endl;
	}

	std::cout << "Final balance after " << NUMBER_OF_ITERATIONS
		  << " iterations" << std::endl;
	r.printTotals();
	std::cout << std::endl;

	return 0;
}
