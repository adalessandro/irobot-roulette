#include <iomanip>
#include <iostream>

#include "player.h"
#include "roulette.h"

Player::Player(std::string name)
	: balance(0), name_(name)
{
	resetBet();
}

Player::~Player()
{
}

void Player::resetBet()
{
	/* Player's bet list (a.k.a. notepad) initial state. */
	bets_ = {1, 2, 3, 4};
}

void Player::printBets()
{
	std::cout << "[ ";
	for (auto b = bets_.cbegin(); b != bets_.cend(); ++b)
		std::cout << *b << ' ';
	std::cout << "]";
}

/* Run a bet and evaluate the result. */
void Player::run(Roulette *roulette, unsigned int num)
{
	unsigned int bet = getBet();

	std::cout << "\t" << name_ << ":\t";

	if (eval(roulette, num)) {
		std::cout << "won  ";
		won(bet);
	} else {
		std::cout << "lost ";
		lost(bet);
	}

	std::cout << std::setw(4) << bet
		  << std::setw(10) << balance << "\t";
	printBets();
	std::cout << std::endl;
}

/* Get player's next bet. */
unsigned int Player::getBet()
{
	if (bets_.size() == 1)
		return bets_.front();

	return bets_.front() + bets_.back();
}

void Player::won(unsigned int bet)
{
	balance += bet;

	bets_.push_back(bet);

	if (getBet() > Roulette::MAX_BET)
		resetBet();
}

void Player::lost(unsigned int bet)
{
	balance -= bet;

	/* Player's bet list can't be empty. Thus, reset it. */
	if (bets_.size() < 3) {
		resetBet();
		return;
	}

	bets_.pop_back();
	bets_.pop_front();

	if (getBet() < Roulette::MIN_BET)
		resetBet();
}

bool PlayerRed::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isRed(num);
}

bool PlayerBlack::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isBlack(num);
}

bool PlayerLow::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isLow(num);
}

bool PlayerHigh::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isHigh(num);
}

bool PlayerOdd::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isOdd(num);
}

bool PlayerEven::eval(Roulette *roulette, unsigned int num)
{
	return roulette->isEven(num);
}
