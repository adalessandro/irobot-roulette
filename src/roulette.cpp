#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "roulette.h"

Roulette::Roulette()
{
	srand(time(NULL));
}

Roulette::~Roulette()
{
}

/*
 * Array specifying red cells in roulette. Note that zero should always be
 * considered not red and not black.
 */
bool Roulette::cells_red[CELLS_LEN] = {
	   0,
	1, 0, 1,
	0, 1, 0,
	1, 0, 1,
	0, 0, 1,
	0, 1, 0,
	1, 0, 1,
	1, 0, 1,
	0, 1, 0,
	1, 0, 1,
	0, 0, 1,
	0, 1, 0,
	1, 0, 1,
};

void Roulette::addPlayer(std::unique_ptr<Player> player)
{
	players_.push_back(std::move(player));
}

/*
 * Run an iteration of the roulette. That is, spin roulette and evaluate
 * players' bet.
 */
void Roulette::run()
{
	int num = rand() % CELLS_LEN;

	std::cout << "Roulette num: " << std::setw(2) << num << std::endl;

	for (auto p = players_.cbegin(); p != players_.cend(); ++p)
		(*p)->run(this, num);
}

void Roulette::printTotals()
{
	long int balance = 0;

	for (auto p = players_.cbegin(); p != players_.cend(); ++p)
		balance += (*p)->balance;

	std::cout << "Total balance: " << balance << std::endl;
}

bool Roulette::isRed(unsigned int num)
{
	return cells_red[num];
}

bool Roulette::isBlack(unsigned int num)
{
	return (num != 0) && !cells_red[num];
}

bool Roulette::isLow(unsigned int num)
{
	return (num != 0) && num <= 18;
}

bool Roulette::isHigh(unsigned int num)
{
	return num > 18;
}

bool Roulette::isOdd(unsigned int num)
{
	return num % 2;
}

bool Roulette::isEven(unsigned int num)
{
	return (num != 0) && !(num % 2);
}
