#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <deque>

class Roulette;

/*
 * Player abstract class.
 *
 * Derived classes should implement specific eval() funtions depending on the
 * player's betting algorithm.
 */
class Player
{
public:
	Player(std::string name);
	~Player();

	void printBets();
	void run(Roulette *roulette, unsigned int num);
	long int balance;

private:
	unsigned int getBet();
	void resetBet();
	virtual bool eval(Roulette *roulette, unsigned int num) = 0;
	void won(unsigned int bet);
	void lost(unsigned int bet);

	std::string name_;
	std::deque<unsigned int> bets_;
};

class PlayerRed : public Player
{
public:
	PlayerRed(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

class PlayerBlack : public Player
{
public:
	PlayerBlack(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

class PlayerLow : public Player
{
public:
	PlayerLow(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

class PlayerHigh : public Player
{
public:
	PlayerHigh(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

class PlayerOdd : public Player
{
public:
	PlayerOdd(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

class PlayerEven : public Player
{
public:
	PlayerEven(std::string name) : Player(name) {};

private:
	bool eval(Roulette *roulette, unsigned int num);
};

#endif /* __PLAYER_H__ */
