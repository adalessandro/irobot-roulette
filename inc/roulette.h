#ifndef __ROULETTE_H__
#define __ROULETTE_H__

#include <memory>
#include <vector>

#include "player.h"

class Player;

class Roulette
{
public:
	Roulette();
	~Roulette();

	static constexpr unsigned int MIN_BET = 5;
	static constexpr unsigned int MAX_BET = 4000;
	static constexpr unsigned int CELLS_LEN = 37;

	void addPlayer(std::unique_ptr<Player> player);

	void run();
	void printTotals();

	bool isRed(unsigned int num);
	bool isBlack(unsigned int num);
	bool isLow(unsigned int num);
	bool isHigh(unsigned int num);
	bool isOdd(unsigned int num);
	bool isEven(unsigned int num);

private:
	std::vector<std::unique_ptr<Player>> players_;
	static bool cells_red[CELLS_LEN];
};

#endif /* __ROULETTE_H__ */
